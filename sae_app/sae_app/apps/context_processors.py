from django.core.exceptions import ObjectDoesNotExist
from sae_app.apps.inicio.models import Perfil


def foto_de_usuario(request):
    """Devuelve la foto de perfil del usuario

    :request: TODO
    :returns: TODO

    """
    imagen = None

    try:
        if request.user.is_authenticated:
            perfil = Perfil.objects.get(user=request.user)
            if perfil.foto:
                imagen = '/media/{}'.format(perfil.foto)
            else:
                imagen = '/static/img/busto.png'
        else:
            imagen = None

    except ObjectDoesNotExist:
        imagen = '/static/img/busto.png'

    return imagen


def cargos_del_usuario(request):
    """Devuelve la foto de perfil del usuario

    :request: TODO
    :returns: TODO

    """
    cargos = {}

    try:
        if request.user.is_authenticated:
            perfil = Perfil.objects.get(user=request.user)
            cargos['directivo'] = perfil.es_directivo
            cargos['administrativo'] = perfil.es_administrativo
            cargos['jefe'] = perfil.es_jefe
            cargos['preceptor'] = perfil.es_preceptor
            cargos['profesor'] = perfil.es_profesor
        else:
            cargos = None

    except ObjectDoesNotExist:
        cargos = {
            'directivo': False,
            'administrativo': False,
            'jefe': False,
            'preceptor': False,
            'profesor': False,
            }

    return cargos


def global_processor(request):
    """Procesador de contexto global.

    :request: TODO
    :returns: Un diccionario con variables de contexto globales.

    """
    ctx = {
        'get_foto_usuario': foto_de_usuario(request),
        'es': cargos_del_usuario(request),
        }

    return ctx
