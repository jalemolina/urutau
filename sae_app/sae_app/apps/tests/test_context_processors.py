import pytest
from mixer.backend.django import mixer
from django.test import RequestFactory
from django.contrib.auth.models import User, AnonymousUser

from ..context_processors import foto_de_usuario, global_processor, \
    cargos_del_usuario


pytestmark = pytest.mark.django_db


class TestContextProcessors:
    """docstring for TestContextProcessors"""
    def test_foto_de_usuario_si_existe(self):
        """TODO: Docstring for test_foto_de_usuario_si_existe """
        req = RequestFactory().get('/')
        user = mixer.blend(User)
        url = 'multimedia/Users/{}/unaimagen.png'.format(user.username)
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user,
            foto=url)
        req.user = user
        result = foto_de_usuario(req)
        assert result == '/media/{}'.format(url), \
            'Debe contener la url de la foto de perfil'

    def test_foto_de_usuario_no_existe(self):
        """TODO: Docstring for test_foto_de_usuario_si_existe """
        req = RequestFactory().get('/')
        user = mixer.blend(User)
        url = '/static/img/busto.png'
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user)
        req.user = user
        result = foto_de_usuario(req)
        assert result == url, 'Debe contener la url a "busto.png"'

    def test_foto_de_usuario_perfil_no_existe(self):
        """TODO: Docstring for test_foto_de_usuario_perfil_no_existe """
        req = RequestFactory().get('/')
        user = mixer.blend(User)
        url = '/static/img/busto.png'
        req.user = user
        result = foto_de_usuario(req)
        assert result == url, 'Debe contener la url a "busto.png"'

    def test_foto_de_usuario_anonimo(self):
        """TODO: Docstring for test_foto_de_usuario_anonimo """
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        result = foto_de_usuario(req)
        assert result is None, 'Debería ser "None"'

# #############################################################################

    def test_cargos_del_usuario_perfil_si_existe(self):
        """TODO: Docstring for test_cargos_del_usuario_perfil_si_existe """
        req = RequestFactory().get('/')
        cargos = {
            'directivo': False,
            'administrativo': False,
            'jefe': False,
            'preceptor': False,
            'profesor': True,
            }
        user = mixer.blend(User)
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user,
            es_profesor=True)
        req.user = user
        result = cargos_del_usuario(req)
        assert result.__eq__(cargos) is True, \
            'Debería devolver el diccionario: {}\n'.format(cargos)

    def test_cargos_del_usuario_perfil_no_existe(self):
        """TODO: Docstring for test_cargos_del_usuario_perfil_no_existe """
        req = RequestFactory().get('/')
        user = mixer.blend(User)
        cargos = {
            'directivo': False,
            'administrativo': False,
            'jefe': False,
            'preceptor': False,
            'profesor': False,
            }
        req.user = user
        result = cargos_del_usuario(req)
        assert result.__eq__(cargos) is True, \
            'Debería devolver el diccionario: {}\n'.format(cargos)

    def test_cargos_del_usuario_anonimo(self):
        """TODO: Docstring for test_cargos_del_usuario_anonimo """
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        result = cargos_del_usuario(req)
        assert result is None, 'Debería ser "None"'

    def test_global_processor(self):
        """TODO: Docstring for test_global_processor """
        req = RequestFactory().get('/')
        user = mixer.blend(User)
        url = '/static/img/busto.png'
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user)
        req.user = user
        result = global_processor(req)
        assert 'get_foto_usuario' in result, \
            'Debe existir la key "get_foto_usuario" en el dict que devuelve'
        assert result['get_foto_usuario'] == url, \
            'Debe contener la url de la imagen'
        assert 'es' in result, \
            'Debe existir la key "es" en el dict que devuelve'
