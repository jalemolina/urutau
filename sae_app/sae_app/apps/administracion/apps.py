from django.apps import AppConfig


class AdministracionConfig(AppConfig):
    name = 'sae_app.apps.administracion'
    verbose_name = 'Administración'
