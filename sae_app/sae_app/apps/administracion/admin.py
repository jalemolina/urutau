from django.contrib import admin

from .models import CajaCurricular, Asignatura, Llamado, Mesa, \
    Jefatura

@admin.register(CajaCurricular)
class CajaCurricularAdmin(admin.ModelAdmin):
    '''
        Admin View for CajaCurricular
    '''
    model = CajaCurricular
    list_display = ('__str__',)
    #list_filter = ('',)
    #inlines = [
        #Inline,
    #]
    #raw_id_fields = ('',)
    #readonly_fields = ('',)
    #search_fields = ['']


@admin.register(Asignatura)
class AsignaturaAdmin(admin.ModelAdmin):
    '''
        Admin View for Asignatura
    '''
    model = Asignatura
    list_display = ('anio', '__str__', 'campo', 'get_caja_curricular_titulo',)

    def get_caja_curricular_titulo(self, obj):
        return obj.caja_curricular.titulo

    get_caja_curricular_titulo.short_description = 'Caja Curricular'


@admin.register(Llamado)
class LlamadoAdmin(admin.ModelAdmin):
    '''
        Admin View for Llamado
    '''
    model = Llamado
    list_display = ('inicio', 'fin')


@admin.register(Mesa)
class MesaAdmin(admin.ModelAdmin):
    '''
        Admin View for Mesa
    '''
    model = Mesa
    list_display = ('asignatura', 'fecha', 'hora')


@admin.register(Jefatura)
class JefaturaAdmin(admin.ModelAdmin):
    '''
        Admin View for Jefatura
    '''
    model = Jefatura
    list_display = ('nombre', 'get_apellido_nombre')

    def get_apellido_nombre(self, obj):
        return '{}, {}'.format(obj.jefe.last_name, obj.jefe.first_name)
    # get_apellido_nombre.admin_order_field = 'jefe' # Permite ordenar columnas

    # Renombra la cabecera de la columna
    get_apellido_nombre.short_description = 'Jefe de Depto.'
