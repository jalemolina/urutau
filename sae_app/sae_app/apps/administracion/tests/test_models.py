# -*- coding: utf-8 -*-
import pytest
from mixer.backend.django import mixer
from django.utils import timezone


pytestmark = pytest.mark.django_db


class TestCajaCurricular(object):

    """Docstring for TestCajaCurricular. """

    def test_model_caja_curricular(self):
        obj = mixer.blend('administracion.CajaCurricular')
        assert obj.pk == 1, \
            'Debería crearse una instancia de --> CajaCurricular.'

    def test_caja_curricular_str(self):
        caja = mixer.blend(
            'administracion.CajaCurricular',
            titulo='Tecnicatura en recreos')
        result = caja.__str__()
        assert result == 'Tecnicatura en recreos', \
            'Debería devolver el título de la caja curricular'


class TestAsignatura(object):

    """Docstring for TestAsignatura. """

    def test_model_asignatura(self):
        obj = mixer.blend('administracion.Asignatura')
        assert obj.pk == 1, 'Debería crearse una instancia de --> Asignatura.'

    def test_asignatura_str(self):
        asignatura = mixer.blend(
            'administracion.Asignatura',
            nombre='Matemática')
        result = asignatura.__str__()
        assert result == 'Matemática', \
            'Debería devolver el nombre de la Asignatura'


class TestNovedad(object):

    """Docstring for TestNovedades. """

    def test_model_novedad(self):
        obj = mixer.blend('administracion.Novedad')
        assert obj.pk == 1, 'Debería crearse una instancia de --> Novedad.'

    def test_novedad_str(self):
        novedad = mixer.blend(
            'administracion.Novedad',
            titulo='Novedades del clima')
        result = novedad.__str__()
        assert result == 'Novedades del clima', \
            'Debería devolver el nombre de la Novedad'


class TestLlamado(object):

    """Docstring for TestLlamado. """

    def test_model_llamado(self):
        obj = mixer.blend('administracion.Llamado')
        assert obj.pk == 1, 'Debería crearse una instancia de --> Llamado.'

    def test_llamado_str(self):
        fecha_inicio = timezone.localdate()
        fecha_fin = timezone.localdate() + timezone.timedelta(days=7)
        llamado = mixer.blend(
            'administracion.Llamado',
            inicio=fecha_inicio,
            fin=fecha_fin)
        result = llamado.__str__()
        assert result == '{} - {}'.format(fecha_inicio, fecha_fin), \
            'Debería devolver el rango de fechas del llamado'


class TestMesa(object):

    """Docstring for TestMesa. """

    def test_model_llamado(self):
        obj = mixer.blend('administracion.Mesa')
        assert obj.pk == 1, 'Debería crearse una instancia de --> Mesa.'

    def test_mesa_str(self):
        fecha_mesa = timezone.localdate() + timezone.timedelta(days=7)
        hora_mesa = timezone.localtime() + timezone.timedelta(days=7)
        mesa_de = mixer.blend(
            'administracion.Asignatura',
            nombre='Matemática')
        mesa = mixer.blend(
            'administracion.Mesa',
            asignatura=mesa_de,
            fecha=fecha_mesa,
            hora=hora_mesa)
        result = mesa.__str__()
        assert result == '{}, {}'.format('Matemática', fecha_mesa), \
            'Debería devolver el nombre de la asignatura y el datetime'


class TestJefatura(object):

    """Docstring for TestJefatura. """

    def test_model_llamado(self):
        obj = mixer.blend('administracion.Jefatura')
        assert obj.pk == 1, 'Debería crearse una instancia de --> Jefatura.'

    def test_jefatura_str(self):
        jefatura = mixer.blend(
            'administracion.Jefatura',
            nombre='Ciencias exactas')
        result = jefatura.__str__()
        assert result == 'Ciencias exactas', \
            'Debería devolver el nombre del departamento'
