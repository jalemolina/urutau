import pytest
from django.contrib.auth.models import User, AnonymousUser
from django.test import RequestFactory
from mixer.backend.django import mixer

from .. import views
#from .. import forms


pytestmark = pytest.mark.django_db


class TestNuevaCarreraView:

    """Docstring for TestNuevaCarreraView. """

    def test_anonymous(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('/administracion/nueva_carrera')
        req.user = AnonymousUser()
        resp = views.NuevaCarreraView.as_view()(req)
        assert resp.status_code == 302, \
            'No debería ser accesible por cualquiera'

    def test_registered_user(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('/administracion/nueva_carrera')
        req.user = mixer.blend(User)
        resp = views.NuevaCarreraView.as_view()(req)
        assert resp.status_code == 200, \
            'No debería ser accesible por cualquiera'

