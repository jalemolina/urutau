import pytest
from mixer.backend.django import mixer
from django.contrib.admin.sites import AdminSite

from .. import admin
from ..models import User, Jefatura, Asignatura, CajaCurricular


pytestmark = pytest.mark.django_db


class TestJefaturaAdmin:

    """Docstring for TestJefaturaAdmin. """

    def test_admin_get_apellido_nombre(self):
        site = AdminSite()
        jefatura_admin = admin.JefaturaAdmin(Jefatura, site)
        user = mixer.blend(
            User,
            username='mi.correo@gmail.com',
            last_name='apellido',
            first_name='nombres')

        jefatura = mixer.blend(Jefatura, jefe=user)
        result = jefatura_admin.get_apellido_nombre(jefatura)
        expected = '{}, {}'.format(user.last_name, user.first_name)
        assert result == expected, 'Debería devolver \'apellido, nombre\''


class TestAsignaturaAdmin:

    """Docstring for TestAsignaturaAdmin. """

    def test_admin_get_caja_curricular_titulo(self):
        site = AdminSite()
        asignatura_admin = admin.AsignaturaAdmin(Asignatura, site)
        _titulo='Técnico en Informática Profesional y Personal'
        caja = mixer.blend(
            CajaCurricular,
            titulo=_titulo)

        asignatura = mixer.blend(Asignatura, caja_curricular=caja)
        result = asignatura_admin.get_caja_curricular_titulo(asignatura)
        expected = '{}'.format(_titulo)
        assert result == expected, 'Debería devolver \'titulo\''
