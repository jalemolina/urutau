# -*- coding: utf-8 -*-
from django import forms

from .models import CajaCurricular, Asignatura


class CajaCurricularForm(forms.ModelForm):

    # TODO: Define form fields here
    class Meta:
        model = CajaCurricular
        fields = [
            'titulo',
            'abreviatura',
            'ley',
            'anio_creacion',
            ]
        labels = {
            'titulo': 'Título',
            'abreviatura': 'Abreviatura',
            'ley': 'Ley/Normativa',
            'anio_creacion': 'Año',
            }
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'validate'}),
            'abreviatura': forms.TextInput(attrs={'class': 'validate'}),
            'ley': forms.TextInput(attrs={'class': 'validate'}),
            'anio_creacion': forms.TextInput(attrs={'class': 'validate'}),
            }


class AsignaturaForm(forms.ModelForm):

    # TODO: Define form fields here
    class Meta:
        model = Asignatura
        fields = [
            'nombre',
            'carga_horaria',
            'campo',
            'anio',
            'caja_curricular',
            ]
        labels = {
            'nombre': 'Nombre de la asignatura',
            'carga_horaria': 'Carga horaria',
            'campo': 'Campo del conocimiento',
            'anio': 'Año',
            'caja_curricular': 'Caja curricular',
            }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'validate'}),
            'carga_horaria': forms.TextInput(attrs={'class': 'validate'}),
            'campo': forms.TextInput(attrs={'class': 'validate'}),
            'anio': forms.TextInput(attrs={'class': 'validate'}),
            'caja_curricular': forms.TextInput(attrs={'class': 'validate'}),
            }
