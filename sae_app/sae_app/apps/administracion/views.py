from django.views.generic import TemplateView, ListView, CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy

from .models import CajaCurricular, Asignatura
from .forms import CajaCurricularForm


class NuevaCarreraView(TemplateView):
    """Cargar nueva carrera. """

    template_name = 'nueva_carrera.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(NuevaCarreraView, self).dispatch(request, *args, **kwargs)


class CajaCurricularListView(CreateView):
    model = CajaCurricular
    template_name = "caja_curricular_list.html"
    form_class = CajaCurricularForm
    success_url = reverse_lazy('lista_carreras')

    def get_context_data(self, **kwargs):
        context = super(
            CajaCurricularListView,
            self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        context["object_list"] = self.model.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        if 'carrera_id' in request.POST:
            carrera_id = request.POST['carrera_id']
            if request.POST['action'] == 'delete':
                try:
                    carrera = CajaCurricular.objects.get(pk=carrera_id)
                    respuesta = {'status': 'True', 'carrera_id': carrera.id}
                    carrera.delete()
                    return JsonResponse(respuesta)
                except:
                    respuesta = {'status': 'False'}
                    return JsonResponse(respuesta)
            elif request.POST['action'] == 'update':
                try:
                    carrera = CajaCurricular.objects.get(pk=carrera_id)
                    carrera.titulo = request.POST['titulo']
                    carrera.abreviatura = request.POST['abreviatura']
                    carrera.ley = request.POST['ley']
                    carrera.anio_creacion = request.POST['anio']
                    carrera.save()
                    respuesta = {'status': 'True', 'carrera_id': carrera.id}
                    return JsonResponse(respuesta)
                except:
                    respuesta = {'status': 'False'}
                    return JsonResponse(respuesta)

        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))
