from django.db import models
from django.contrib.auth.models import User


class Llamado(models.Model):

    class Meta:
        verbose_name = "Llamado"
        verbose_name_plural = "Llamados"
        ordering = ['-inicio']

    inicio = models.DateField(
        null=False,
        blank=False)
    fin = models.DateField(
        null=False,
        blank=False)

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Devuelve el rango de fechas.

        """
        return '{} - {}'.format(self.inicio, self.fin)


class Mesa(models.Model):

    class Meta:
        verbose_name = "Mesa"
        verbose_name_plural = "Mesas"
        ordering = ['-llamado__inicio', 'fecha', 'hora', 'asignatura']

    asignatura = models.ForeignKey('administracion.Asignatura', on_delete=models.CASCADE)

    REGULARES = 'REGULARES'
    PREVIOS = 'PREVIOS'
    ESPECIALES = 'ESPECIALES'
    ALUMNOS_CHOICES = (
        (REGULARES, 'regulares'),
        (PREVIOS, 'previos'),
        (ESPECIALES, 'especiales'),
    )

    de_alumnos = models.CharField(
        max_length=10,
        choices=ALUMNOS_CHOICES,
        default=REGULARES)
    fecha = models.DateField(
        null=False,
        blank=False)
    hora = models.TimeField(
        null=False,
        blank=False)

    llamado = models.ForeignKey(Llamado, on_delete=models.CASCADE)
    presidente = models.ForeignKey(
        User,
        related_name='presidente',
        null=True,
        blank=True, on_delete=models.CASCADE)
    vocal1 = models.ForeignKey(
        User,
        related_name='vocal_1',
        null=True,
        blank=True, on_delete=models.CASCADE)
    vocal2 = models.ForeignKey(
        User,
        related_name='vocal_2',
        null=True,
        blank=True, on_delete=models.CASCADE)
    suplente = models.ForeignKey(
        User,
        related_name='suplente',
        null=True,
        blank=True, on_delete=models.CASCADE)

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Devuelve el rango de fechas.

        """
        return '{}, {}'.format(self.asignatura.nombre, self.fecha)


class Novedad(models.Model):

    class Meta:
        verbose_name = "Novedad"
        verbose_name_plural = "Novedades"
        ordering = ['-creado']

    titulo = models.CharField(
        max_length=50,
        null=False,
        blank=False)
    descripcion = models.CharField(
        max_length=180,
        null=False,
        blank=False)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Título de la novedad.

        """
        return self.titulo


class CajaCurricular(models.Model):

    class Meta:
        verbose_name = "Caja curricular"
        verbose_name_plural = "Cajas curriculares"
        ordering = ['titulo']

    titulo = models.CharField(
        max_length=100,
        null=False,
        blank=False)
    abreviatura = models.CharField(
        max_length=10,
        null=False,
        blank=False)
    ley = models.CharField(
        max_length=100,
        null=True,
        blank=True)
    anio_creacion = models.PositiveSmallIntegerField(
        verbose_name='año de creación',
        null=True,
        blank=True)

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Título de la caja curricular.

        """
        return self.titulo


class Asignatura(models.Model):

    class Meta:
        verbose_name = "Asignatura"
        verbose_name_plural = "Asignaturas"
        ordering = ['caja_curricular', 'anio', 'campo', 'nombre']

    FORMACION_GENERAL = 'FORMACION_GENERAL'
    FORMACION_CIENTIFICO_TECNOLOGICO = 'FORMACION_CIENTIFICO_TECNOLOGICO'
    FORMACION_TECNICA_ESPECIFICA = 'FORMACION_TECNICA_ESPECIFICA'
    PRACTICAS_PROFESIONALIZANTES = 'PRACTICAS_PROFESIONALIZANTES'
    CAMPOS_CHOICES = (
        (FORMACION_GENERAL, 'General'),
        (FORMACION_CIENTIFICO_TECNOLOGICO, 'Formacion Cientifico Tecnologico'),
        (FORMACION_TECNICA_ESPECIFICA, 'Formacion Tecnica Especifica'),
        (PRACTICAS_PROFESIONALIZANTES, 'Practicas Profesionalizantes'),
    )
    nombre = models.CharField(
        max_length=70,
        null=False,
        blank=False)
    carga_horaria = models.IntegerField(
        null=True,
        blank=True)
    campo = models.CharField(
        max_length=50,
        choices=CAMPOS_CHOICES,
        null=True,
        blank=True)
    anio = models.PositiveSmallIntegerField(verbose_name='año',)
    caja_curricular = models.ForeignKey(CajaCurricular, on_delete=models.CASCADE)

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Nombre de la Asignatura.

        """
        return self.nombre


class Jefatura(models.Model):

    class Meta:
        verbose_name = "Jefatura"
        verbose_name_plural = "Jefaturas"
        ordering = ['nombre']

    nombre = models.CharField(max_length=70)
    jefe = models.ForeignKey(User, related_name='jefe', on_delete=models.CASCADE)
    asignaturas = models.ManyToManyField(Asignatura)
    profesores = models.ManyToManyField(User, related_name='profesores')

    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: Devuelve el nombre del departamento.

        """
        return self.nombre
