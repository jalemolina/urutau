from django.conf.urls import include, url
from sae_app.apps.administracion import views


urlpatterns = [
    url(r'^administracion/lista_carreras$',
        views.CajaCurricularListView.as_view(),
        name="lista_carreras"),
    ]
