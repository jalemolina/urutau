from django.shortcuts import render
from django.template import RequestContext


def error_400_view(request, exception):
    """TODO: Docstring for error_400_view.
    """
    # data = {}
    response = render(
        request,
        './errors/400.html',
        # data)
        None
    )
    response.status_code = 400
    return response


def error_403_view(request, exception):
    """TODO: Docstring for error_403_view.
    """
    response = render(
        None,
        './errors/403.html',
        None
    )
    response.status_code = 403
    return response


def error_404_view(request, exception):
    """TODO: Docstring for error_404_view.
    """
    response = render(
        None,
        './errors/404.html',
        None
    )
    response.status_code = 404
    return response


def error_500_view(request):
    """TODO: Docstring for error_500_view.
    """
    response = render(
        None,
        './errors/500.html'
    )
    response.status_code = 500
    return response
