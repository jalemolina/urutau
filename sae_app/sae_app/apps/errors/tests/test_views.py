from unittest import mock
from django.core.exceptions import SuspiciousOperation, PermissionDenied
from django.http import Http404
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
import pytest

from sae_app.apps.inicio.views import LoginView
from sae_app import urls
from .. import views


pytestmark = pytest.mark.django_db


class TestErrorHandlers:

    """Testea todos los manajadores personalizados de error que se hayan
    configurado."""

    def raise_(exception):
        def wrapped(*args, **kwargs):
            raise exception('Test exception')
        return wrapped

    def test_login_page(self):
        req = RequestFactory().get('/login')
        req.user = AnonymousUser()
        resp = LoginView.as_view()(req)
        assert resp.status_code == 200, (
            'Debería estar disponible con codigo de estado 200')

    @mock.patch(
        'sae_app.apps.inicio.views.LoginView.as_view',
        raise_(SuspiciousOperation)
        )
    def test_Bad_Request_400(self):
        """TODO: Docstring for test_anonymous.
        """
        assert urls.handler400.endswith('.error_400_view'), (
            'Debería apuntar a la vista correspondiente'
            )
        req = RequestFactory().get('/login')
        req.user = AnonymousUser()
        resp = views.error_400_view(req, None)
        assert resp.status_code == 400, 'Debería emitir un status_code 400'
        assert 'Solicitud incorrecta' in resp.serialize().decode('utf8'), (
            'La plantilla debería contener el texto: Solicitud incorrecta'
            )

    @mock.patch(
        'sae_app.apps.inicio.views.LoginView.as_view',
        raise_(PermissionDenied)
        )
    def test_Forbidden_403(self):
        """TODO: Docstring for test_anonymous,w.
        """
        assert urls.handler403.endswith('.error_403_view'), (
            'Debería apuntar a la vista correspondiente'
            )
        req = RequestFactory().get('/login')
        resp = views.error_403_view(req, None)
        assert resp.status_code == 403, 'Debería emitir un status_code 403'
        assert 'Acceso denegado' in resp.serialize().decode('utf8'), (
            'La plantilla debería contener el texto: Acceso denegado'
            )

    @mock.patch(
        'sae_app.apps.inicio.views.LoginView.as_view',
        raise_(Http404)
        )
    def test_call_inexistent_page_return_404(self):
        """TODO: Docstring for test_anonymous.
        """
        assert urls.handler404.endswith('.error_404_view'), (
            'Debería apuntar a la vista correspondiente'
            )
        req = RequestFactory().get('/login')
        resp = views.error_404_view(req, None)
        assert resp.status_code == 404, 'Debería emitir un status_code 404'
        assert 'Página no encontrada' in resp.serialize().decode('utf8'), (
            'La plantilla debería contener el texto: Página no encontrada'
            )

    @mock.patch(
        'sae_app.apps.inicio.views.LoginView.as_view',
        views.error_500_view(RequestFactory().get('/login'))
        )
    def test_internal_server_error_return_500(self):
        """TODO: Docstring for test_anonymous.
        """
        assert urls.handler500.endswith('.error_500_view'), (
            'Debería apuntar a la vista correspondiente'
            )
        req = RequestFactory().get('/login')
        resp = views.error_500_view(req)
        assert resp.status_code == 500, 'Debería emitir un status_code 500'
        assert 'Problema interno' in resp.serialize().decode('utf8'), (
            'La plantilla debería contener el texto: Problema interno'
            )
