from django.apps import AppConfig


class PreceptoriaConfig(AppConfig):
    name = 'sae_app.apps.preceptoria'
    verbose_name = 'preceptoría'
