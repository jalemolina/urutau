from django.contrib import admin

from .models import Estudiante


@admin.register(Estudiante)
class AnioAdmin(admin.ModelAdmin):
    '''
        Admin View for Anio
    '''
    model = Estudiante
    list_display = ('__str__', 'dni', 'image_tag',)
