from django.db import models
from django.utils.html import format_html


class Estudiante(models.Model):

    class Meta:
        verbose_name = "Estudiante"
        verbose_name_plural = "Estudiantes"

    nombre = models.CharField(
        max_length=50,
        null=False,
        blank=False)
    apellido = models.CharField(
        max_length=25,
        null=False,
        blank=False)
    dni = models.PositiveIntegerField(
        null=False,
        blank=False,
        unique=True)

    def media_url(self, filename):
        """Establece la url para guardar la foto del estudiante

        :filename: Nombre del archivo de imagen.
        :returns: Ruta donde guardar la foto del estudiante.

        """
        ruta = 'multimedia/Students/{}_{}'.format(self.dni, filename)
        return ruta

    foto = models.ImageField(
        upload_to=media_url,
        null=True,
        blank=True)

    def image_tag(self):
        """TODO: Docstring for image_tag.
        :returns: TODO

        """
        if self.foto:
            fotito = '/media/{}'.format(self.foto)
        else:
            fotito = '/static/img/busto.png'
        return format_html(
            "<image src='{}' style='height:100px'/>".format(fotito))

    def __str__(self):
        return '{}, {}'.format(self.apellido, self.nombre)
