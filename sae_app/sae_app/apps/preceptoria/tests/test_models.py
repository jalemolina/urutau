# -*- coding: utf-8 -*-
import pytest
from mixer.backend.django import mixer


pytestmark = pytest.mark.django_db


class TestEstudiante(object):

    """Docstring for TestEstudiante """

    dni = 66666666
    img = 'unaimagen.png'

    def test_model_estudiante(self):
        obj = mixer.blend('preceptoria.Estudiante')
        assert obj.pk == 1, \
            'Debería crearse una instancia de --> Estudiante'

    def test_media_url(self):
        """TODO: Docstring for test_media_url """
        student = mixer.blend(
            'preceptoria.Estudiante',
            dni=self.dni)
        result = student.media_url(self.img)
        assert result == 'multimedia/Students/{}_{}'.format(
            self.dni,
            self.img), \
            'Debe devolver la ruta donde se guardará la imagen '

    def test_image_tag_exists_foto(self):
        """TODO: Docstring for test_image_tag_exists_foto """
        url = 'multimedia/Students/{}_{}'.format(self.dni, self.img)
        estudiante = mixer.blend(
            'preceptoria.Estudiante',
            dni=self.dni,
            foto=url)
        result = estudiante.image_tag()
        should = "<image src='/media/{}' style='height:100px'/>".format(url)
        assert result == should, \
            'Debe devolver un html tag (image) con el src apuntando a la foto'

    def test_image_tag_dont_exists_foto(self):
        """TODO: Docstring for test_image_tag_dont_exists_foto """
        estudiante = mixer.blend(
            'preceptoria.Estudiante',
            dni=self.dni)
        url = '/static/img/busto.png'
        result = estudiante.image_tag()
        should = "<image src='{}' style='height:100px'/>".format(url)
        assert result == should, \
            '''Debe devolver un html tag (image) con el src apuntando a la
            imagen "busto.png"'''

    def test_estudiante_str(self):
        estudiante = mixer.blend(
            'preceptoria.Estudiante',
            nombre='Carlos Pepito',
            apellido='Sultán')
        result = estudiante.__str__()
        assert result == 'Sultán, Carlos Pepito', \
            'Debería devolver el apellido y el nombre del estudiante'
