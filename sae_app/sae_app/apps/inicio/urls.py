from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from sae_app.apps.inicio import views
from .forms import RegisterForm


urlpatterns = [
    url(r'^$',
        views.HomeView.as_view(),
        name="home"),
    url(r'^login$',
        auth_views.LoginView.as_view(template_name='login.html',
                                     extra_context={"form_register":RegisterForm}),
        name='login'),
    url(r'^logout/$', auth_views.logout_then_login, name='logout'),
    url(r'^register/$',
        views.RegistrationView.as_view(),
        name='register'),
    url(r'^register/success/$',
        views.RegistrationSuccessView.as_view(),
        name='register_success'),
    ]
