import pytest
from django.contrib.auth.models import User
from mixer.backend.django import mixer
from .. import forms


pytestmark = pytest.mark.django_db


class TestRegistrationForm:

    """Docstring for TestRegistrationForm """

    def test_form(self):
        form = forms.RegisterForm(data={})
        assert form.is_valid() is False, (
            'Debería ser inválido si no contiene datos')

        form = forms.RegisterForm(
                data={'email': 'micorreo@gmail.com',
                      'password': '123456',
                      'confirm_password': '12345',
                      'nombre': 'Un Nombre',
                      'apellido': 'Un Apellido'})
        assert form.is_valid() is False, (
            'Deberían ser inválido si no coinciden las contraseñas')

        form = forms.RegisterForm(
                data={'email': 'micorreo@gmail',
                      'password': '123456',
                      'confirm_password': '123456',
                      'nombre': 'Un Nombre',
                      'apellido': 'Un Apellido'})
        assert form.is_valid() is False, (
            'Debería ser inválido si la dirección de email no es válida')

        form = forms.RegisterForm(
                data={'email': 'micorreo@gmail.com',
                      'password': '123456',
                      'confirm_password': '123456',
                      'nombre': '',
                      'apellido': ''})
        assert form.is_valid() is False, (
            'Debería ser inválido si faltan datos')

        form = forms.RegisterForm(
                data={'email': 'micorreo@gmail.com',
                      'password': '123456',
                      'confirm_password': '123456',
                      'nombre': 'Un Nombre',
                      'apellido': 'Un Apellido'})
        assert form.is_valid() is True, (
            'Debería ser váildo al cargar correctamente los datos')

    @pytest.mark.django_db
    def test_form_save_method(self):
        """
        test form saves name, surname, email values to corresponding User
        object when commiting form
        """
        registerform = forms.RegisterForm(
            data={'email': 'micorreo@server.com',
                  'password': '123456',
                  'confirm_password': '123456',
                  'nombre': 'Un Nombre',
                  'apellido': 'Un Apellido'})

        assert registerform.is_valid() is True, (
            'Formulario de registro no es válido')
        registerform.save()
        new_user = User.objects.get(username='micorreo@server.com')

        assert new_user.email == "micorreo@server.com", (
            'No se guardó el usuario')
