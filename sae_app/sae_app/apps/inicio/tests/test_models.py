import pytest
from mixer.backend.django import mixer
from ..models import User


pytestmark = pytest.mark.django_db


class TestPerfil:
    """docstring for TestPerfil"""
    def test_model(self):
        obj = mixer.blend('inicio.Perfil')
        assert obj.pk == 1, 'Debe haber una instancia'

    def test_media_url(self):
        """TODO: Docstring for test_media_url """
        user = mixer.blend(User)
        perfil = mixer.blend('inicio.Perfil', user=user)
        result = perfil.media_url('unaimagen.png')
        assert result == 'multimedia/Users/{}/unaimagen.png'.format(
            user.username), \
            'Debe devolver la ruta donde se guardará la imagen '

    def test_image_tag_exists_foto(self):
        """TODO: Docstring for test_image_tag_exists_foto """
        user = mixer.blend(User)
        url = 'multimedia/Users/{}/unaimagen.png'.format(user.username)
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user,
            foto=url)
        result = perfil.image_tag()
        should = "<image src='/media/{}' style='height:100px'/>".format(url)
        assert result == should, \
            'Debe devolver un html tag (image) con el src apuntando a la foto'

    def test_image_tag_dont_exists_foto(self):
        """TODO: Docstring for test_image_tag_dont_exists_foto """
        user = mixer.blend(User)
        url = '/static/img/busto.png'
        perfil = mixer.blend(
            'inicio.Perfil',
            user=user)
        result = perfil.image_tag()
        should = "<image src='{}' style='height:100px'/>".format(url)
        assert result == should, \
            '''Debe devolver un html tag (image) con el src apuntando a la
            imagen "busto.png"'''

    def test_perfil_str(self):
        """TODO: Docstring for test_perfil_str """
        user = mixer.blend(User, username='Alejandro')
        perfil = mixer.blend('inicio.Perfil', user=user)
        result = perfil.__str__()
        assert result == 'Alejandro', 'Debe devolver el nombre de usuario'
