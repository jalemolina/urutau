import pytest
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from django.urls import reverse, resolve
from mixer.backend.django import mixer

from .. import views
from .. import forms


pytestmark = pytest.mark.django_db


class TestLoginView:

    """Docstring for TestLoginView. """

    def test_anonymous(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('/login')
        req.user = AnonymousUser()
        resp = views.LoginView.as_view()(req)
        assert resp.status_code == 200, 'Debería ser accesible por cualquiera'


class TestRegistrationView:

    """Docstring for TestRegistrationView. """

    def test_anonymous(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('/register')
        req.user = AnonymousUser()
        resp = views.RegistrationView.as_view()(req)
        assert resp.status_code == 200, 'Debería ser accesible por cualquiera'

    def test_form_valid(self):
        dataa = {'email': 'micorreo@server.com',
                 'password': '123456',
                 'confirm_password': '123456',
                 'nombre': 'Un Nombre',
                 'apellido': 'Un Apellido'}
        req = RequestFactory().post('/register', dataa)
        resp = views.RegistrationView.as_view()(req)
        assert 'success' in resp.url, 'Debería redireccionar success'


class TestRegistrationSuccessView:

    """Docstring for TestLoginView. """

    def test_anonymous(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('register/success/')
        req.user = AnonymousUser()
        resp = views.RegistrationView.as_view()(req)
        assert resp.status_code == 200, 'Debería ser accesible por cualquiera'


class TestHomeView:

    """Docstring for TestHomeView. """

    def test_anonymous(self):
        """TODO: Docstring for test_anonymous.
        """
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        resp = views.HomeView.as_view()(req)
        assert 'login' in resp.url, 'Debería redireccionar al login'

    def test_anyuser(self):
        user = mixer.blend('auth.User', is_registered_user=True)
        req = RequestFactory().get('/')
        req.user = user
        resp = views.HomeView.as_view()(req)
        assert resp.status_code == 200, (
            'Debería ser accesible por un usuario registrado')

    def test_superuser(self):
        user = mixer.blend('auth.User', is_superuser=True)
        req = RequestFactory().get('/')
        req.user = user
        resp = views.HomeView.as_view()(req)
        assert resp.status_code == 200, 'Debería ser accesible por superuser'
