from django.test import RequestFactory
from django.urls import reverse, resolve


class TestUrls:

    def test_logout_url(self):
        path = reverse('logout')
        assert resolve(path).view_name == 'logout'
