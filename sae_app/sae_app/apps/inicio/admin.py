from django.contrib import admin

from .models import Perfil


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    '''
        Admin View for Perfil
    '''
    model = Perfil
    list_display = ('__str__', 'image_tag',)
    #list_filter = ('',)
    #inlines = [
        #Inline,
    #]
    #raw_id_fields = ('',)
    #readonly_fields = ('',)
    #search_fields = ['']
