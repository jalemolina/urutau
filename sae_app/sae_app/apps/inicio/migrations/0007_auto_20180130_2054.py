# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-30 20:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inicio', '0006_auto_20180130_2052'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='es_administrativo',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='perfil',
            name='es_jefe',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='perfil',
            name='es_preceptor',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='perfil',
            name='es_profesor',
            field=models.BooleanField(default=False),
        ),
    ]
