from django.db import models
from django.contrib.auth.models import User
from django.utils.html import format_html


class Perfil(models.Model):
    class Meta:
        verbose_name = "perfil"
        verbose_name_plural = "perfiles"

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    fecha_de_nacimiento = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)
    telefono = models.CharField(
        max_length=15,
        null=True,
        blank=True)
    dni = models.PositiveIntegerField(
        null=True,
        blank=True,
        unique=True)
    es_directivo = models.BooleanField(
        default=False,
        null=False,
        blank=False)
    es_administrativo = models.BooleanField(
        default=False,
        null=False,
        blank=False)
    es_jefe = models.BooleanField(
        default=False,
        null=False,
        blank=False)
    es_preceptor = models.BooleanField(
        default=False,
        null=False,
        blank=False)
    es_profesor = models.BooleanField(
        default=False,
        null=False,
        blank=False)

    def media_url(self, filename):
        """Establece la url para guardar la foto del usuario

        :filename: Nombre del archivo de imagen.
        :returns: Ruta donde guardar la foto de perfil.

        """
        ruta = 'multimedia/Users/{}/{}'.format(self.user.username, filename)
        return ruta

    foto = models.ImageField(
        upload_to=media_url,
        null=True,
        blank=True)

    def image_tag(self):
        """TODO: Docstring for image_tag.
        :returns: TODO

        """
        if self.foto:
            fotito = '/media/{}'.format(self.foto)
        else:
            fotito = '/static/img/busto.png'
        return format_html(
            "<image src='{}' style='height:100px'/>".format(fotito))

    def __str__(self):
        return self.user.username
