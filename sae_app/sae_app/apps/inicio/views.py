from django.views.generic import TemplateView, RedirectView
from django.views.generic.edit import FormView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import logout as auth_logout
from .forms import RegisterForm


class LoginView(TemplateView):
    template_name = "login.html"


class RegistrationSuccessView(TemplateView):
    template_name = "register_success.html"


class RegistrationView(FormView):
    template_name = "register.html"
    form_class = RegisterForm
    success_url = 'success/'

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)


class HomeView(TemplateView):
    """Pagina de inicio. """

    template_name = 'home.html'

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(HomeView, self).dispatch(request, *args, **kwargs)
