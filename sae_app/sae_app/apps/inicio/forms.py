# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User


class RegisterForm(forms.Form):

    email = forms.EmailField(
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'validate',
                   # 'placeholder': 'Correo electrónico',
                   'autofocus': ''}),
        help_text='email')
    password = forms.CharField(
        max_length=30,
        required=True,
        label='Contraseña:',
        help_text='lock',
        widget=forms.PasswordInput(
            attrs={'class': 'validate', 'id':'id_password_reg'}))
    confirm_password = forms.CharField(
        max_length=30,
        required=True,
        label='Confirmar contraseña:',
        help_text='lock',
        widget=forms.PasswordInput(
            attrs={'class': 'validate'}))
    nombre = forms.CharField(
        max_length=150,
        help_text='account_circled',
        required=True)
    apellido = forms.CharField(
        max_length=150,
        required=True)

    def save(self):
        """TODO: Docstring for save.
        """
        data = self.cleaned_data
        user = User.objects.create_user(
            username=data['email'],
            email=data['email'],
            first_name=data['nombre'],
            last_name=data['apellido'],
            password=data['password'],
            is_active=False)
        us = user.save()
        return us


    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        password = cleaned_data.get('password')
        confirmed_password = cleaned_data.get('confirm_password')

        if password and confirmed_password and password != confirmed_password:
            msg = "Las contraseñas no coinciden."
            self.add_error('password', msg)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            user = User.objects.get(username=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError('El email ya fue registrado.')
