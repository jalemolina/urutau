"""sae_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView


FAVICON_VIEW = RedirectView.as_view(url='/static/LogoTecnica.gif',
                                    permanent=True)

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^', include("sae_app.apps.inicio.urls")),
    url(r'^', include("sae_app.apps.administracion.urls")),
    # Soluciona el problema del error 404 para favicon.ico
    url(r'^favicon\.ico$', FAVICON_VIEW),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler400 = 'sae_app.apps.errors.views.error_400_view'
handler403 = 'sae_app.apps.errors.views.error_403_view'
handler404 = 'sae_app.apps.errors.views.error_404_view'
handler500 = 'sae_app.apps.errors.views.error_500_view'

# Texto para poner al final del <title> de cada página.
admin.site.site_title = u'Sistema de Administración Escolar'

# Texto a poner en los <h1> de todas las páginas.
admin.site.site_header = u'Sistema de Administración Escolar'

# Texto a poner arriba de la página de index del admin
admin.site.index_title = u'Panel de control de SAE'
