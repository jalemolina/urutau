Urutaú - Sistema de Administración Escolar
========
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Version: 0.0.1](https://img.shields.io/badge/Version-0.0.1-blue.svg)](https://gitlab.com/jalemolina/urutau)
[![coverage](https://gitlab.com/jalemolina/urutau/badges/master/coverage.svg?job=coverage&style=flat)](https://gitlab.com/jalemolina/urutau/commits/master)

Completar esto!!!


---

 [![Made with: Python](https://img.shields.io/badge/made%20with-Python-3776AB?logo=python&style=for-the-badge)](https://www.python.org/)
 [![Made with: Django](https://img.shields.io/badge/made%20with-Django-092E20?logo=django&style=for-the-badge)](https://www.djangoproject.com/)
 [![Urutaú](https://img.shields.io/badge/uruta%C3%BA%20on-wikipedia-000000?logo=wikipedia&style=for-the-badge)](https://es.wikipedia.org/wiki/Nyctibius_griseus)
